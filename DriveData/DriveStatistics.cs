﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace DriveData
{
    public class DriveStatistics
    {
        public DateTime StartDate;
        public DateTime EndDate;
        public TimeSpan TotalTime {
            get { return this.EndDate.Subtract(StartDate); }
        }

        public double DistanceTraveled;

        public double AvgSpeed;
        public double MaxSpeed = 0;


        public float AccEco = 0;
        public float BreakEco = 0;

        public List<Location> Data;
    }
}