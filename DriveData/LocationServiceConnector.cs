﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace DriveData
{
    public class LocationServiceConnector : Java.Lang.Object, IServiceConnection
    {
        static readonly string TAG = typeof(LocationServiceConnector).FullName;

        MainActivity mainActivity;
        public LocationServiceConnector(MainActivity activity)
        {
            IsConnected = false;
            Binder = null;
            mainActivity = activity;
        }

        public bool IsConnected { get; private set; }
        public LocationServiceBinder Binder { get; private set; }

        public void OnServiceConnected(ComponentName name, IBinder service)
        {
            Binder = service as LocationServiceBinder;
            IsConnected = this.Binder != null;

            string message = "onServiceConnected - ";
            //    Log.Debug(TAG, $"OnServiceConnected {name.ClassName}");

            if (IsConnected)
            {
                message = message + " bound to service " + name.ClassName;
                //        mainActivity.UpdateUiForBoundService();
            }
            else
            {
                message = message + " not bound to service " + name.ClassName;
                //        mainActivity.UpdateUiForUnboundService();
            }

            Log.Info(TAG, message);
            //    mainActivity.timestampMessageTextView.Text = message;

        }

        public void OnServiceDisconnected(ComponentName name)
        {
            Log.Debug(TAG, $"OnServiceDisconnected {name.ClassName}");
            IsConnected = false;
            Binder = null;
            //    mainActivity.UpdateUiForUnboundService();
        }
    }
}