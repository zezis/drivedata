﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;
using Android.Gms.Location;
using Android.Support.V4.Content;
using Android;
using Android.Content.PM;
using Android.Support.V4.App;
using System;
using System.Timers;
using System.Linq;
using Android.Util;
using System.IO;
using Android.Graphics;
using Path = System.IO.Path;

namespace DriveData
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : AppCompatActivity
    {
        static readonly string TAG = typeof(MainActivity).FullName;
        LocationServiceConnector serviceConnection;

        bool running = false;
        TextView statTextView, timeTV, startTimeTV, speedTV, avgSpeedTV, maxSpeedTV, distanceTV, breakStatTV, accStatTV, debugTV;
        Button startBtn, exportBtn;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            //this.statTextView = FindViewById<TextView>(Resource.Id.text);


            this.speedTV = FindViewById<TextView>(Resource.Id.speedtext);
            this.timeTV = FindViewById<TextView>(Resource.Id.timetext);
            this.startTimeTV = FindViewById<TextView>(Resource.Id.timestart);


            this.distanceTV = FindViewById<TextView>(Resource.Id.distance);

            this.maxSpeedTV = FindViewById<TextView>(Resource.Id.maxspeed);
            this.avgSpeedTV = FindViewById<TextView>(Resource.Id.avgspeed);

            this.breakStatTV = FindViewById<TextView>(Resource.Id.breakstatus);
            this.accStatTV = FindViewById<TextView>(Resource.Id.accstatus);

            this.debugTV = FindViewById<TextView>(Resource.Id.debugtext);


            this.startBtn = FindViewById<Button>(Resource.Id.button);
            this.startBtn.Click += (s, o) => {
                //   Android.Locations.Location location = await new FusedLocationProviderClient(this).GetLastLocationAsync();
                if (running) DoUnBindService();
                else DoBindService();
            };

            this.exportBtn = FindViewById<Button>(Resource.Id.exportbutton);
            this.exportBtn.Click += (s, o) => { ExportData(); };


            var t = new Timer(1000);
            t.Elapsed += (o, a) => this.RunOnUiThread(() => this.UpdateUIFromService());
            t.Start();
        }

        protected override void OnStart()
        {
            base.OnStart();
            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.AccessFineLocation) != Permission.Granted) {
                ActivityCompat.RequestPermissions(this, new string[] { Manifest.Permission.AccessFineLocation }, 0);
            }

            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) != Permission.Granted)
            {
                ActivityCompat.RequestPermissions(this, new string[] { Manifest.Permission.WriteExternalStorage }, 0);
            }

            if (serviceConnection == null) serviceConnection = new LocationServiceConnector(this);
        }

        protected override void OnDestroy()
        {
            Log.Debug(TAG, "OnDestroy");
            DoUnBindService();
            base.OnDestroy();
        }

        void UpdateUIFromService() {
            if (running && this.serviceConnection != null)
            {
                var c = this.serviceConnection.Binder?.Service?.Locations?.Any();
                if (c.HasValue && c.Value == true)
                {
                    var s = this.serviceConnection.Binder.Service;
                    var stats = s.CalculateStats();
                    float speed = (s.Locations.Last().Speed / 1000 * 60 * 60);
                    speedTV.Text = Math.Round(speed, 0) + "";



                    var timeFS = s.TimeFromStart;
                    timeTV.Text = $"{timeFS.Hours.ToString("00")}:{timeFS.Minutes.ToString("00")}:{timeFS.Seconds.ToString("00")}";
                    startTimeTV.Text = s.StartDate.ToString("HH:mm");

                    this.avgSpeedTV.Text = Math.Round(stats.AvgSpeed, 1) + " km/h";
                    this.maxSpeedTV.Text = Math.Round(stats.MaxSpeed, 0) + " km/h";

                    this.distanceTV.Text = Math.Round(stats.DistanceTraveled / 1000, 1) + " km";
                    this.accStatTV.Text = Math.Round(stats.AccEco * 100, 0)+"%";
                    this.breakStatTV.Text = Math.Round(stats.BreakEco * 100, 0) + "%";

                    this.debugTV.Text = "Location Samples: " + stats.Data.Count
                        + "\nFrequency: " + Math.Round(timeFS.TotalSeconds/(double)stats.Data.Count,1) +"s"
                        + "\nTime:"+stats.TotalTime.TotalSeconds
                        + "\nStart timestamp: "+stats.StartDate.ToString("HH:mm")
                        + "\nEnd timestamp: " + stats.EndDate.ToString("HH:mm");


                }
                else speedTV.Text = "-";
            }
        }

        void DoBindService()
        {
            Intent serviceToStart = new Intent(this, typeof(LocationService));
            if(!running)BindService(serviceToStart, serviceConnection, Bind.AutoCreate);
            running = true;
            this.startBtn.Text = "Stop";
            this.startBtn.SetTextColor(Color.ParseColor("#ff0000"));

            this.debugTV.Text = "";
            this.timeTV.Text = "";

            this.maxSpeedTV.Text = "-";
            this.avgSpeedTV.Text = "-";
            this.distanceTV.Text = "-";
            this.breakStatTV.Text = "-";
            this.accStatTV.Text = "-";
        }

        void DoUnBindService()
        {
            if(running) UnbindService(serviceConnection);
            running = false;
            this.startBtn.Text = "Start";
            this.startBtn.SetTextColor(Color.Green);

            this.speedTV.Text = "-";
        }


        void ExportData() {
            var date = DateTime.Now;
            var backingFile = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath, $"export-{date.Year}{date.Month}{date.Day}-{date.Hour}{date.Minute}.csv");
            
            using (var writer = File.CreateText(backingFile))
            {
                writer.WriteLine("Time;Latitude;Longitude;Speed;Accuracy;Provider;DistanceToPrevious");
                Android.Locations.Location prev = null;
                float distanceToPrev = 0;
                foreach (var l in this.serviceConnection.Binder.Service.Locations) {
                    if (prev != null) distanceToPrev = l.DistanceTo(prev);//{l.SpeedAccuracyMetersPerSecond};
                    writer.WriteLine($"{l.Time};{l.Latitude};{l.Longitude};{l.Speed};{l.Accuracy};{l.Provider};{distanceToPrev}");
                    prev = l;
                }
            }
            Toast.MakeText(Application.Context, "Exported as "+backingFile, ToastLength.Long).Show();
        }
    }
}