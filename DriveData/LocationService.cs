﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Util;
using Android.Views;
using Android.Widget;

using Android.Gms.Location;
using Android.Locations;
using Java.Util;

namespace DriveData
{
    [Service]
    public class LocationService : Service
    {
        static readonly string TAG = typeof(LocationService).FullName;
        static readonly int NOTIFICATION_ID = 10000;
      
        FusedLocationProviderClient fusedLocationProviderClient;

        public List<Location> Locations = new List<Location>();
        public DateTime StartDate = DateTime.Now;
        public TimeSpan TimeFromStart { get { return DateTime.Now.Subtract(StartDate); }}
        public double DistanceTraveled = 0;

        public IBinder Binder { get; private set; }

        public override IBinder OnBind(Intent intent)
        {
            Log.Debug(TAG, "OnBind");
            this.Binder = new LocationServiceBinder(this);
            this.Notification("DriveData", "DriveData is getting information about your drive...");

            fusedLocationProviderClient = LocationServices.GetFusedLocationProviderClient(this);

            LocationRequest r = new LocationRequest()
                .SetPriority(LocationRequest.PriorityHighAccuracy)
                .SetInterval(500)
                .SetFastestInterval(100);
            fusedLocationProviderClient.RequestLocationUpdatesAsync(r, new LocationServiceCallback(this));

            return this.Binder;
        }

        public override bool OnUnbind(Intent intent)
        {
            // This method is optional to implement
            Log.Debug(TAG, "OnUnbind");
            return base.OnUnbind(intent);
        }

        public override void OnDestroy()
        {
            // This method is optional to implement
            Log.Debug(TAG, "OnDestroy");
            CancelNotification();
            Binder = null;
            base.OnDestroy();
        }


        public override void OnCreate()
        {
            base.OnCreate();
            Log.Info(TAG, "OnCreate: the service is initializing.");
        }


        void Notification(string title, string text)
        {
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .SetSmallIcon(Android.Resource.Drawable.IcDialogInfo)
                .SetContentTitle(title)
                .SetContentText(text)
                .SetOngoing(true);

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(NOTIFICATION_ID, notificationBuilder.Build());
        }

        void CancelNotification() {
            var nm = NotificationManager.FromContext(this);
            nm.CancelAll();
        }


        public DriveStatistics CalculateStats() {
            var res = new DriveStatistics();
            res.StartDate = UnixTimeStampToDateTime(Locations.First()?.Time/1000.0 ?? 0);
            res.EndDate = UnixTimeStampToDateTime(Locations.Last()?.Time/1000.0 ?? 0);
            res.Data = this.Locations;
            res.DistanceTraveled = this.DistanceTraveled;

            Location prev = null;
            double totalSpeed = 0;
            float maxSpeed = 0;

            const float minAccuracy = 20;
            const float maxDelay = 5;
            const float bestAccValue = 0.3f;
            const float worstAccValue = 1;
            const float bestBreakValue = -0.3f;
            const float worstBreakValue = -3f;

            float accKSum = 0;
            float accSum = 0;

            float breakKSum = 0;
            float breakSum = 0;


            foreach (var next in Locations) {
                maxSpeed = Math.Max(maxSpeed, next.Speed);

                if (prev != null) {
                    double elapsed = new DateTime(next.Time).Subtract(new DateTime(prev.Time)).TotalSeconds;
                    if (elapsed < maxDelay && prev.Accuracy < minAccuracy && next.Accuracy < minAccuracy) {
                        float change = (next.Speed - prev.Speed);
                        float changePerS = change / (float)elapsed;


                        if (changePerS > 0)
                        {   //akcelerace
                            accKSum += ((changePerS - bestAccValue) / worstAccValue) * change;
                            accSum += change;
                        }
                        else if (changePerS < 0) {
                            breakKSum += ((changePerS - bestBreakValue) / worstBreakValue) * change;
                            breakSum += change;
                        }
                    }
                }
                prev = next;
            }

            res.AccEco = Math.Min(0,Math.Max(1, accKSum / accSum));
            res.BreakEco = Math.Min(0, Math.Max(1, breakKSum / breakSum));
            res.MaxSpeed = maxSpeed;
            res.AvgSpeed = res.DistanceTraveled / res.TotalTime.TotalHours;
            return res;
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}