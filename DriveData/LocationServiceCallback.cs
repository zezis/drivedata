﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Location;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace DriveData
{
    public class LocationServiceCallback: LocationCallback
    {
        private LocationService service;
        public LocationServiceCallback(LocationService service) {
            this.service = service;
        }

        public override void OnLocationAvailability(LocationAvailability locationAvailability)
        {
            Log.Debug("FusedLocationProviderSample", "IsLocationAvailable: {0}", locationAvailability.IsLocationAvailable);
        }

        public override void OnLocationResult(LocationResult result)
        {
            if (result.Locations.Any())
            {
                var location = result.Locations.First();
                Log.Debug("Sample", $"The latitude is : {location.Latitude}, speed: {location.Speed}, accuracy: {location.Accuracy}");
                this.service.Locations.Add(location);

                if (this.service.Locations.Any()){
                    this.service.DistanceTraveled += this.service.Locations.Last().DistanceTo(result.Locations.First());
                }
            }
            else
            {
                // No locations to work with.
            }
        }
    }
}